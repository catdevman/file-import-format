# Goals
* [ ]  Given a csv file detect if file has header row (are all values in first row strings?) - 
* [x]  Given a csv file detect each field type (Use reflection to go to most specific to least - date, boolean, number, string)
* [x]  Acceptable date formats must be given up front and not dates will be seen as strings
* [x]  After detecting, give output in yaml of each columns type, possible header, etc...

# Possible helpful resources
- easycsv - https://godoc.org/github.com/yunabe/easycsv
- gocsv - https://github.com/gocarina/gocsv
- csv golang std lib - https://golang.org/pkg/encoding/csv/
- detect date format - https://github.com/araddon/dateparse
- str conv - https://golang.org/pkg/strconv/
- csv mulitple core -  http://github.com/mzimmerman/multicorecsv

# Build steps

`go build main.go` will output a `main` executable file.

`go run main.go` will run it without having to build the executable each time.

# Options

```
Describe a csv file format in yaml

Usage:
  file-import-format [flags]

Flags:
      --config string        config file (default is $HOME/.file-import-format.yaml)
  -r, --header               File has a header
  -h, --help                 help for file-import-format
  -i, --import-file string   File to create format for (default "import.csv")
```