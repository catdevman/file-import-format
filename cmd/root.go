/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"

	"github.com/mzimmerman/multicorecsv"

	"gopkg.in/yaml.v3"
)

var cfgFile string

type output struct {
	Columns []column `yaml:"columns"`
}

type column struct {
	Name  string `yaml:"name"`
	Index int    `yaml:"index"`
	Type  string `yaml:"type"`
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "file-import-format",
	Short: "Describe a csv file format in yaml",
	Long:  ``,
	// Uncomment the following line if your bare application
	// has an action associated with it
	Run: func(cmd *cobra.Command, args []string) {
		header, err := cmd.Flags().GetBool("header")
		if err != nil {
			fmt.Println(err)
			return
		}

		i, err2 := cmd.Flags().GetString("import-file")
		if err2 != nil {
			fmt.Println(err2)
			return
		}

		// Open the file
		csvfile, err := os.Open(i)
		if err != nil {
			log.Fatalln("Couldn't open the csv file", err)
		}

		var rows [][]ValueType
		var headers []string
		r := multicorecsv.NewReader(csvfile)
		defer r.Close() // the underlying strings.Reader cannot be closed,
		// but that doesn't matter, multicorecsv needs to clean up
		var index int64
		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatal(err)
			}

			if index == 0 && header {
				for _, v := range record {
					headers = append(headers, strings.TrimSpace(v))
				}
			} else {
				rows = append(rows, processRow(record))
			}
			if index >= 50 {
				break
			}

			index++
		}

		var c int
		if len(headers) > 0 {
			c = len(headers)
		} else {
			c = len(rows[0])
		}

		columns := make([][]ValueType, c)

		for _, r := range rows {
			for ci, v := range r {
				columns[ci] = append(columns[ci], v)
			}
		}

		columnTypes := processColumnTypes(columns)

		var co []column
		for a, ct := range columnTypes {
			var col column
			col.Index = a
			if len(headers) > 0 {
				col.Name = headers[a]
			} else {
				col.Name = "index_" + strconv.Itoa(a)
			}
			col.Type = ct
			co = append(co, col)
		}
		out := output{
			Columns: co,
		}

		bt, err := yaml.Marshal(out)

		if err != nil {
			fmt.Println("problem with changing output to yaml")
		}

		fmt.Println(string(bt))

	},
}

func processColumnTypes(columns [][]ValueType) (colTypes []string) {
	for _, col := range columns {
		var potColTypes = make(map[string]int)
		for _, val := range col {
			potColTypes[val.Type]++
		}
		var highestTypeCount int
		var highestType string
		for key, value := range potColTypes {
			if value > highestTypeCount {
				highestTypeCount = value
				highestType = key
			}
		}
		colTypes = append(colTypes, highestType)
	}

	return colTypes
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.file-import-format.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("header", "r", false, "File has a header")
	rootCmd.Flags().StringP("import-file", "i", "import.csv", "File to create format for")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".file-import-format" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".file-import-format")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
		fmt.Println(viper.GetBool("header"))
	}
}
