package cmd

import (
	"strconv"
	"strings"

	"github.com/araddon/dateparse"
)

// ValueType used to identify value type and format of a record
type ValueType struct {
	Value  interface{}
	Type   string
	Format string
}

func processRow(record []string) (ret []ValueType) {
	for _, value := range record {
		v := strings.TrimSpace(value)
		t := detectType(v)
		ret = append(ret, t)
	}
	return ret
}

func detectType(value string) ValueType {
	// date
	t, err := dateparse.ParseFormat(value)
	if err == nil {
		parsed, _ := dateparse.ParseAny(value)
		return ValueType{
			Value:  parsed,
			Type:   "date",
			Format: t,
		}
	}
	// float
	_, err = strconv.ParseFloat(value, 64)

	if err == nil && strings.ContainsAny(value, ".") {
		parsed, _ := strconv.ParseFloat(value, 64)
		return ValueType{
			Value: parsed,
			Type:  "float",
		}
	}
	// int
	_, err = strconv.ParseInt(value, 10, 64)
	if err == nil {
		parsed, _ := strconv.ParseInt(value, 10, 64)
		return ValueType{
			Value: parsed,
			Type:  "int",
		}
	}

	// boolean
	_, err = strconv.ParseBool(value)
	if err == nil {
		parsed, _ := strconv.ParseBool(value)
		return ValueType{
			Value: parsed,
			Type:  "bool",
		}
	}

	// string
	return ValueType{
		Value:  value,
		Type:   "string",
		Format: "",
	}
}
